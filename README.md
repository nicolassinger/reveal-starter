# Reveal.js Starter pour GitLab CI

Projet de démarrage pour utiliser GitLab CI, les GitLab Pages et Reveal.js.

## Utilisation

Pour créer une nouvelle présentation : 
- placez-vous dans le dossier `presentations` et faites une copie du dossier `exemple`. Appelez cette copie selon le nom de votre
nouvelle présentation. 
- Dans le nouveau dossier, éditez le fichier `index.html` pour paramétrer les propriétés de votre présentation (nom, auteur, etc.) et éventuellement modifier
certains de ses paramètres (par exemple la largeur et la hauteur de l'écran afin de définir son ratio).
- Editez le fichier `slides.md' pour saisir le contenu des différentes diapositives de votre présentation.
- Vous pouvez utiliser le fichier `custom.css` pour définir votre propre styles CSS et y faire référence dans vos diapositives.
- Dans le fichier `index.html` à la racine du projet, ajoutez une entrée à la table des matières HTML correspondant à
votre nouvelle présentation.
- Vous pouvez visualiser votre présentation en saisissant en ligne de commande `http-server` et en pointant votre navigateur
sur une des adresses indiquées lors de son démarrage (si la commande n'existe pas, saisissez `npm install` et réessayer)
- Faites un `commit` de votre projet et poussez-le sur GitLab, vos présentations seront disponibles sur votre espace 
[GitLab Page](https://docs.gitlab.com/ee/user/project/pages/) au bout d'un URL de format (*username* est à remplacer par votre nom
utilisateur GitLab et *projet* est à rassembler par le nom du projet chez GitLab) https://*username*.gitlab.io/*projet*/presentations

## Crédits

[Reveal.js](https://github.com/hakimel/reveal.js)
