##### Jetpack Compose

![logo](backgrounds/compose.png)  <!-- .element height="20%" width="20%" -->

Développement Android

*Nicolas Singer*
---
#### Compose sert à construire des interfaces utilisateurs natives avec Android
- version stable en 2022
- remplace la définition des interfaces au format XML
- logique basée sur la décomposition en composants
---
#### Approche composants
- Compose permet de définir des composants qui définissent leur affichage 
- Cette définition se fait par du code Kotlin.

<div class="twocolumn">
<div>

```kotlin
@Composable
fun Screen() {
  Text(
          text = "Des fleurs",
          style = MaterialTheme.typography.h4,
          modifier = Modifier.padding(10.dp)
)
  Image(
          painterResource(id = R.drawable.fleur),
          contentDescription = "Des fleurs",
          modifier = Modifier.size(200.dp)
)
```
</div>
<div>

![logo](backgrounds/fleurs.png)  <!-- .element height="50%" width="50%" -->
</div>
</div>
---
#### Approche composants
Cette approche permet la décomposition du rendu  

```kotlin
@Composable
fun Screen() {
    Texte()
    MonImage()
}

@Composable
fun Texte(){
    Text(
        text = "Des fleurs",
        style = MaterialTheme.typography.h4,
        modifier = Modifier.padding(10.dp)
    )
}

@Composable
fun MonImage() {
    Image(
        painterResource(id = R.drawable.fleur),
        contentDescription = "Des fleurs",
        modifier = Modifier.size(200.dp)
    )
}
```
